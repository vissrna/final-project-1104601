<!DOCTYPE html>
<html>
    <head>
        <title>Start Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <link href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script>
            var model = null;
            var dlgModel = null;
            $(document).ready(function () {
                dlgModel = new Vue({
                    el: "#dlg",
                    data: {
                        type: "",
                        id: "",
                        password: ""
                    }
                });
                $.ajax("logins", {
                    success: function (data) {
                        model = new Vue({
                            el: "#app",
                            data: {
                                logins: data
                            },
                            methods: {
                                deleteLogin: function (i) {
                                    $.ajax("login_info?id="+model.logins[i].id, {
                                        type: "DELETE",
                                        success: function(d){
                                            model.logins.splice(i, 1);
                                        }
                                    });
                                },
                                editLogin: function (o) {
                                    dlgModel.type = o.type;
                                    dlgModel.id = o.id;
                                    dlgModel.password = o.password;
                                    $("#dlg").dialog({
                                        modal: true,
                                        buttons: [
                                            {
                                                text: "OK",
                                                click: function () {
                                                    $.ajax("login_info?type"+dlgModel.type+"&id="+dlgModel.id+"&password="+dlgModel.password, {
                                                        type: "PUT",
                                                        success: function(d){
                                                            o.type = dlgModel.type;
                                                            o.id = dlgModel.id;
                                                            o.password = dlgModel.password;
                                                        }
                                                    });
                                                    
                                                    $(this).dialog("close");
                                                }
                                            }
                                        ]
                                    });
                                }
                            }
                        });
                    }
                });
            });

            function addLogin() {
                dlgModel.type = "";
                dlgModel.id = "";
                dlgModel.password = "";
                $("#dlg").dialog({
                    modal: true,
                    buttons: [
                        {
                            text: "input",
                            click: function () {
                                $.ajax("login_info", {
                                    type: "POST",
                                    data: {
                                        type: dlgModel.type,
                                        id: dlgModel.id,
                                        password: dlgModel.password
                                    },
                                    success: function (d) {
                                        model.logins.push({
                                            type: dlgModel.type,
                                            id: dlgModel.id,
                                            password: dlgModel.password
                                        });
                                    }
                                });
                                $(this).dialog("close");
                            }
                        }
                    ]
                });
            }
        </script>
    </head>
    <body>
                <%
            String pic = "question";
            pic=(String)session.getAttribute("sess");
            if(pic==null){
                pic="question";
            }
        %>
        <img id="foodImage" style="position: absolute; left: 10px; top: 10px; width: 200px" src="<%=pic%>.png"></img>
        
        </br></br></br></br></br></br></br></br></br></br></br></br>
        <form action="setFood" method="POST">
                &emsp;Background Choose: <select name="food">
                    <option value="tree1">Tree1</option>
                    <option value="tree2">Tree2</option>
                    <option value="tree3">Tree3</option>
                </select>
            <input type="submit"></input>
        </form>
        </br></br></br>
        
        <button onclick="addLogin();">AddRecord</button>
        <table id="app" border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Money</th>
                    <th>Note</th>
                    <th>Edit</th>
                    <th>Del</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(login, index) in logins">
                    <td>{{index}}</td>
                    <td>{{login.type}}</td>
                    <td>{{login.id}}</td>
                    <td>{{login.password}}</td>
                    
                    <td><button v-on:click="editLogin(login);">Edit</button></td>
                    <td><button v-on:click="deleteLogin(index);">Delete</button></td>
                </tr>
            </tbody>
        </table>
        <div id="dlg" style="display:none">
            income or expenditure: <input type="text" v-model="type"/><br/><br/>
            money: <input type="text" v-model="id"/><br/><br/>
            notice: <input type="text" v-model="password"/><br/><br/>
        </div>
        

        </p>
        
        <!--
        <td style="text-align:center">sum:</td>
        -->
    </body>
</html>
