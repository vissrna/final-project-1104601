/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.loginws;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author lendle
 */
@WebServlet(name = "LoginInfoServlet", urlPatterns = {"/login_info"})
public class LoginInfoServlet extends HttpServlet {
    
    private void getImpl1(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        System.out.println("getImpl1");
        response.setContentType("application/json");
        try (PrintWriter out=response.getWriter(); Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/test", "root", "")) {
            String id=request.getParameter("id");
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery("select * from test where id='"+id+"'");
            Map map=new HashMap();
            if(rs.next()){
                map.put("id", rs.getString("id"));
                map.put("password", rs.getString("password"));
                map.put("type", rs.getString("type"));
            }
            Gson gson=new Gson();
            out.print(gson.toJson(map));
            //////////////////////////////
        }catch(Exception e){
            throw new ServletException(e);
        }
    }
    
    private void getImpl2(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        System.out.println("getImpl2");
        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out=response.getWriter(); Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/test", "root", "")) {
            //re-implement getImpl1
            //this time reponde in json-format
            //and use gson
            String id=request.getParameter("id");
            
            //////////////////////////////
        }catch(Exception e){
            throw new ServletException(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getImpl1(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doPut");
        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out=response.getWriter(); 
                Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/test", "root", "")) {
            //update the corresponding user
            String type=request.getParameter("type");
            String id=request.getParameter("id");
            String password=request.getParameter("password");
            PreparedStatement pstmt=conn.prepareStatement(
                    "update test set password=? where id=?");
            pstmt.setString(1, id);
            pstmt.setString(2, password);
            pstmt.setString(3, type);
            if(pstmt.executeUpdate()==1){
                out.print("success");
            }else{
                out.print("fail");
            }
        }catch(Exception e){
            throw new ServletException(e);
        }
    }
    
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doDelete");
        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out=response.getWriter(); 
                Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/test", "root", "")) {
            //delete the corresponding user
            String id=request.getParameter("id");
            Statement s=conn.createStatement();
            int ret=s.executeUpdate("delete from test where id='"+id+"'");
            if(ret==1){
                out.print("success");
            }else{
                out.print("fail");
            }
        }catch(Exception e){
            throw new ServletException(e);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doPost");
        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out=response.getWriter(); Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/test", "root", "")) {
            //insert the corresponding user
            String type=request.getParameter("type");
            String id=request.getParameter("id");
            String password=request.getParameter("password");
            
            PreparedStatement ps=conn.prepareStatement(
                    "insert into login (type,id, password) values (?,?,?)");
            ps.setString(1, id);
            ps.setString(2, password);
            ps.setString(3, type);
            if(ps.executeUpdate()==1){
                out.println("success");
            }else{
                out.println("fail");
            }
            //////////////////////////////
            
        }catch(Exception e){
            throw new ServletException(e);
        }
    }

}
